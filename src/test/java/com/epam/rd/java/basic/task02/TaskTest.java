package com.epam.rd.java.basic.task02;

import static org.junit.jupiter.api.Assertions.*;

import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.*;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

public class TaskTest {
	
	private static final String UTIL_PACK_NAME = "java.util";

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ParameterizedTest
	@CsvFileSource(resources = "data.csv", delimiter = ';')
	void test(String listClassSimpleName, String methodName, int n, int k, int expected) throws Exception {
		Method m = Task.class.getMethod(methodName, List.class, int.class);
		Class listClass = Class.forName(UTIL_PACK_NAME + '.' + listClassSimpleName);
		List<Integer> list = getList(listClass, n);
		m.invoke(null, list, k);
		int actual = list.get(0);
		assertEquals(expected, actual);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private List<Integer> getList(Class<? extends List> listClass, int n) {
		return IntStream.range(0, n)
			.boxed()
			.collect(Collectors.toCollection(() -> {
				List<Integer> list = null;
				try {
					list = listClass.getDeclaredConstructor().newInstance();
				} catch (ReflectiveOperationException e) {
					e.printStackTrace();
				}
				return list;
			}));
	}

}